const chai = require("chai");
const { expect } = require("chai");
const chaiHttp = require("chai-http");
const testConstants = require("./_test.constants");
let server = require("../app");
const db = require('../utils/database');
const should = require("chai").should();
chai.use(chaiHttp);
chai.use(require("chai-sorted"));
const checkUser = (item) => {
  expect(item).to.have.property("id");
  expect(item).to.have.property("username");
  expect(item).to.have.property("email");
  expect(item).to.have.property("firstname");
  expect(item).to.have.property("lastname");
  expect(item).to.have.property("birthdate");
  expect(item).to.have.property("description");
  expect(item).to.have.property("imgurl");
};

let specificUserId;
before( function(done) {
  setTimeout(()=>{done()},100)
});
describe("/api/users", () => {

  it("should post new user", (done) => {
    chai
      .request(server)
      .post("/api/users")
      .send(testConstants.generateUser())
      .end((err, res) => {
        const { body, statusCode } = res;
        expect(statusCode).to.equal(201);
        checkUser(body);
        specificUserId = body.id;
        done();
      });
  });
  it("should not post new user without username", (done) => {
    chai
      .request(server)
      .post("/api/users")
      .send(testConstants.generateInvalidUser())
      .end((err, res) => {
        const { body, statusCode } = res;
        expect(statusCode).to.equal(400);
        done();
      });
  });
  it("should get all users", (done) => {
    chai
      .request(server)
      .get("/api/users")
      .end((err, res) => {
        const { body, statusCode } = res;
        expect(statusCode).to.equal(200);
        body.forEach((item) => {
          checkUser(item);
        });
        done();
      });
  });
  it("should get specific user", (done) => {
    chai
      .request(server)
      .get(`/api/users/${specificUserId}`)
      .end((err, res) => {
        const { body, statusCode } = res;
        expect(statusCode).to.equal(200);
        checkUser(body);
        done();
      });
  });
  it("GET ONE - should get error for invalid id", (done) => {
    chai
      .request(server)
      .get('/api/users/invalidId')
      .end((err, res) => {
        const { body, statusCode } = res;
        expect(statusCode).to.equal(400);
        done();
      });
  });
  it("should update user", (done) => {
    chai
      .request(server)
      .put(`/api/users/`)
      .send({id:specificUserId, username :'newUsername'})
      .end((err, res) => {
        const { body, statusCode } = res;
        expect(statusCode).to.equal(200);
        checkUser(body);
        done();
      });
  });
  it("should delete user", (done) => {
    chai
      .request(server)
      .delete(`/api/users/${specificUserId}`)
      .end((err, res) => {
        const { body, statusCode } = res;
        expect(statusCode).to.equal(204);
        done();
      });
  });
  it("DELETE - should give error for invalid id", (done) => {
    chai
      .request(server)
      .delete('/api/users/invalidId')
      .end((err, res) => {
        const {  statusCode } = res;
        expect(statusCode).to.equal(400);
        done();
      });
  });
});
