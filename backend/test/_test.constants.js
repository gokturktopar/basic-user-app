const Chance = require('chance');
const chance = new Chance();


module.exports = {
    generateUser : () => ({
      username: chance.name(),
      email: chance.email(),
      firstname: chance.name(),
      lastname: chance.last(),
      birthdate: chance.date(),
      description: chance.string(),
      imgurl: chance.url(),
    }),
    generateInvalidUser : () => ({
      email: chance.email(),
      firstname: chance.name(),
      lastname: chance.last(),
      birthdate: chance.date(),
      description: chance.string(),
      imgurl: chance.url(),
    })
}
