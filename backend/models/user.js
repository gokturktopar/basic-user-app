
const { Sequelize, DataTypes } = require('sequelize');

module.exports = (sequelize)=> sequelize.define('User', {
  id: {
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false,
    primaryKey: true
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  email: {
    type: DataTypes.STRING,
    unique: true
  },
  firstname: {
    type: DataTypes.STRING
  },
  lastname: {
    type: DataTypes.STRING
  },
  birthdate: {
    type: DataTypes.DATEONLY
  },
  description: {
    type: DataTypes.STRING
  },
  imgurl: {
    type: DataTypes.STRING
  }
}, {
  sequelize,
  modelName: 'User'
});
