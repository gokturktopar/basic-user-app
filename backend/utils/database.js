const { Sequelize } = require("sequelize");
const path = require("path");
const user = require("../models/user");
const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: path.resolve(__dirname, "../data/dev.sqlite3"),
});

const UserModel = user(sequelize, Sequelize);

sequelize
  .sync({ force: true })
  .then(() => {
    console.log("Database ready for usage...");
  })
  .catch((err) => console.log(err));

module.exports = {
  UserModel,
  sequelize
};
