const { UserModel} = require('../utils/database')
const errors = require("../utils/error");

const getAll = async () => {
  try {
    const users = await UserModel.findAll();
    return users;
  } catch (e) {
    throw errors.BackendError(e);
  }
};

const getOne = async (id) => {
  try {
    const user = await UserModel.findByPk(id);
    if(!user){
      throw errors.BackendError({message:'No User Found!'});
    }
    return user;
  } catch (e) {
    throw errors.BackendError(e);
  }
};

const create = async (user) => {
  try {
    const newUser = await UserModel.create(user);
    return newUser;
  } catch (e) {
    throw errors.BackendError(e);
  }
};

const deleteById = async (id) => {
  try {
    let deletedUser = await getOne(id);
    const result = await deletedUser.destroy();
     return result;
  } catch (e) {
    throw errors.BackendError(e);
  }
};

const updateById = async (user) => {
  try {
    let updatedUser = await getOne(user.id);
   const result = await updatedUser.update({...user});
    return result;
  } catch (e) {
    throw errors.BackendError(e);
  }
};

module.exports = {
  create,
  getAll,
  getOne,
  updateById,
  deleteById,
};
