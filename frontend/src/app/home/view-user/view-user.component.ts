import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '@app/services/user.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  @Input() id : string;
  @Output() output = new EventEmitter();
  loadingPage: boolean = true;
  visiblity: boolean = true;
  viewData: any;
  customerName: any;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.loadingPage = true;
    this.getCustomer(this.id);
  }

  getCustomer(id: any) {
    this.loadingPage = true;
    this.userService.getOne(id).pipe(finalize(() => { this.loadingPage = false })).subscribe(data => {
      this.viewData = data;
    })
  }

  dialogClosed() {
    this.visiblity = false
    this.output.emit()
  }

}
