import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewUserComponent } from './view-user.component';

import { SidebarModule } from 'primeng/sidebar';
import { BlockUIModule } from 'primeng/blockui';
@NgModule({
  declarations: [ViewUserComponent],
  exports: [ViewUserComponent],

  imports: [
    CommonModule,
    BlockUIModule,
    SidebarModule
  ]
})
export class ViewUserModule { }
