import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateUserComponent } from './create-user.component';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ToastModule } from 'primeng/toast';
import { MessageModule } from 'primeng/message';
import { DropdownModule } from 'primeng/dropdown';
import { DataViewModule } from 'primeng/dataview';
import { CardModule } from 'primeng/card';
import { FileUploadModule } from 'primeng/fileupload';
import { ProgressBarModule } from 'primeng/progressbar';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { PanelModule } from 'primeng/panel';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TableModule } from 'primeng/table';
import { KeyFilterModule } from 'primeng/keyfilter';
import { SidebarModule } from 'primeng/sidebar';
import { BlockUIModule } from 'primeng/blockui';

@NgModule({
  declarations: [CreateUserComponent],
  exports: [CreateUserComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    KeyFilterModule,
    CardModule,
    ToastModule,
    PanelModule,
    TableModule,
    MessageModule,
    DataViewModule,
    CalendarModule,
    DropdownModule,
    InputTextModule,
    FileUploadModule,
    ProgressBarModule,
    InputTextareaModule,
    SidebarModule,
    BlockUIModule,
  ]
})
export class CreateUserModule { }
