import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { User } from 'src/app/models/user'
import { forkJoin } from 'rxjs';
import { MessageService } from 'primeng/api';
import { UserService } from '@app/services/user.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent implements OnInit {
  @Input() id : string;
  @Output() output = new EventEmitter();
  user = new User()
  postForm: FormGroup;
  loadingSpinner: boolean = false;
  visibility: boolean = true;
  addForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,

  ) {
    this.postForm = this.fb.group({
      username: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required]),
      firstname: new FormControl(null, null),
      lastname: new FormControl(null, null),
      birthdate: new FormControl(null, null),
      description: new FormControl(null, null),
      imgurl: new FormControl(null, null),
    });
  }

  ngOnInit(): void {
    if(this.id){
      this.getCustomer()
    }
  }
  getCustomer() {
    this.loadingSpinner = true;
    this.userService.getOne(this.id).pipe(finalize(() => { this.loadingSpinner = false })).subscribe(data => {
      this.user = data;
    })
  }
  save(){
    if (this.id) {
      this.update();
    } else {
      this.post()
    }
  }
  update() {
    this.loadingSpinner = true;
    this.user.birthdate = this.user.birthdate ? new Date(this.user.birthdate) : null;
    this.userService.update(this.user).subscribe(data => {
      this.dialogClosed(1);
    },
      error => {
        this.dialogClosed(0);
      })
  }
  post() {
    this.loadingSpinner = true;
    delete this.user.id;

    this.user.birthdate = this.user.birthdate ? new Date(this.user.birthdate) : null;
    this.userService.post(this.user).subscribe(data => {
      this.dialogClosed(1);
    },
      error => {
        this.dialogClosed(0);
      })
  }

  dialogClosed(state: number) {
    this.visibility = false;
    this.output.emit(state)
  }

}
