import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import {User} from '../models/user';
import { UserService } from '@app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [MessageService],

})
export class HomeComponent implements OnInit {
  users : User[] = []
  cols : Object[] = []
  isLoading = false;
  sortedBy: string = 'avaragePace'
  sortDirection: string = 'ASC'
  groupBy: boolean = false
  isCreateModalActive = false;
  isOpenModalActive = false;
  isDeleteModalActive = false
  deleteUserId: string
  viewUserId: string
  updateUserId :string = null
  constructor(
    private userService: UserService,
    private messageService: MessageService,

  ) { }

  ngOnInit() {
    this.cols  = [
      { field: 'username', header: 'Username',  width: 'auto'},
      { field: 'email', header: 'Email', width: 'auto'},
      { field: 'firstname', header: 'Firstname', width: 'auto'},
      { field: 'lastname', header: 'Lastname', width: 'auto' },
      { field: 'birthdate', header: 'Birthdate', width: 'auto' },
      { field: 'description', header: 'Description', width: 'auto' },
      { field: 'imgurl', header: 'Image Url',  width: 'auto' },
      { field: 'actions', header: '',  width: 'auto' },
    ]
    this.getAll();
  }
  getAll() {
    this.isLoading = true
    this.userService.getAll().pipe(finalize(() => { this.isLoading = false })).subscribe(data => {
      this.users = data
    },
      error => { this.errorMessage('Users Cant Retrieved!') })
  }
  deleteUser() {
    this.isLoading = true
    this.userService.delete(this.deleteUserId).pipe(finalize(() => { this.isLoading = false })).subscribe(data => {
      this.successMessage('User Deleted!')
      this.getAll();
    },
      error => { this.errorMessage('User Not Deleted!') })
  }
  prepareForCreateModel(id :string){
    this.updateUserId = id;
    this.isCreateModalActive = true
  }
  getOutputFromCreateModal(result: number){
    this.isCreateModalActive = false;
    if (result == 1) {
      this.successMessage('User Saved')
      this.getAll();
    } else if(result == 0){
      this.errorMessage('User Not Saved')
    }
  }
  prepareForViewModel(id :string){
    this.viewUserId = id;
    this.isOpenModalActive = true
  }
  openWarning(id :string) {
    this.deleteUserId = id
    this.messageService.clear();
    this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Are you sure?', detail: 'Do you want to proceed' });
  }
  onConfirm() {
    this.deleteUser()
    this.messageService.clear('c');
    this.isDeleteModalActive= false;
  }
  onReject() {
    this.isDeleteModalActive= false;
    this.messageService.clear('c');
  }
  successMessage=(detail: string)=> {
    this.messageService.add(
      { severity: 'success', summary: 'Success Message', detail: detail }
    );
  }
  errorMessage(detail : string) {
    this.messageService.add(
      { severity: 'error', summary: 'Error Message', detail: detail }
    );
  }
}
