
export class User {
  id: string
  username: string
  email: string
  firstname: string
  lastname: string
  birthdate: Date
  description: string
  imgurl: string
  constructor() {
    this.id = null
    this.username = null
    this.email = null
    this.firstname = null
    this.lastname = null
    this.birthdate = null
    this.description = null
    this.imgurl = null
  }
};

